import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('등산 사랑회 소개'),
        backgroundColor: Colors.green,
        centerTitle: true, // 중앙 정렬
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.menu), // 햄버거버튼 아이콘 생성
          onPressed: () {
            // 아이콘 버튼 실행
            print('menu button is clicked');
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart), // 장바구니 아이콘 생성
            onPressed: () {
              // 아이콘 버튼 실행
              print('Shopping cart button is clicked');
            },
          ),
          IconButton(
            icon: Icon(Icons.search), // 검색 아이콘 생성
            onPressed: () {
              // 아이콘 버튼 실행
              print('Search button is clicked');
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset('assets/ho1.jpg',
              width: 300,
              height: 250,
              fit: BoxFit.fill,),
            Container(
              child: const Column(
                children: [
                  Text(
                      '밥풀이 소개',
                      style: TextStyle(
                        letterSpacing: 2.0,
                      ),
                  ),
                  Text('밥풀이는 멋져'),
                ],
              ),
            ),

            Container( width:500 ,
              height: 500,
              margin:const EdgeInsets.all(30) ,
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset('assets/ho2.jpg',
                      width: 150,
                      height: 150,
                      fit: BoxFit.fill,),
                      Image.asset('assets/ho3.jpg',
                        width: 150,
                        height: 150,
                        fit: BoxFit.fill,),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset('assets/ho4.jpg',
                        width: 150,
                        height: 150,
                        fit: BoxFit.fill,),
                      Image.asset('assets/ho5.jpg',
                        width: 150,
                        height: 150,
                        fit: BoxFit.fill,),
                    ],
                  )
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
